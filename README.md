### **`bin/aur.sh`**
Simple script to install and search for [AUR](https://aur.archlinux.org/) packages.
My chalenge with that script was to parse the user arguments in a smart way, I
thing my solution could be better somehow, but that's still working and is quite
easy to understand the code.

#### Help message
```
Usage: aur.sh [OPTION] [ARGUMENT]
Simple tool to search and install AUR packages

Arguments:
  -i --install [REPO] Clone to /tmp and install the package
  -s --search [QUERY] To open librewolf in AUR search page
```

#### Enviroment variables
- `$EDITOR`: to open the *`PKGBUILD`* file
