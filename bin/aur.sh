#!/bin/env bash

# function to show help message and exit
help() {
    echo "Usage: aur.sh [OPTION] [ARGUMENT]"
    echo "Simple tool to search and install AUR packages"
    echo
    echo "Arguments:"
    echo "  -i --install [REPO] Clone to /tmp and install the package"
    echo "  -s --search [QUERY] To open librewolf in AUR search page"
    exit "$1"
}

# function to show a nice looking log output
btrlog() {
    printf "\e[4%sm \e[;;3${1}m %s\e[m\n" "$1" "$2"
}

# argument parser
ARGS=$(getopt -o "i:sh" -l "install:,search,help" -n "$0" -- "$@")
[ $? -ne 0 ] && help 1
eval set -- "$ARGS"

# default options
install=false
install_repo=
search=false
search_query=

# validation of the user arguments and options
while true; do
    case $1 in
        -h | --help) help 0 ;;
        \?) help 1 ;;
        -i | --install)
            install=true
            install_repo=$2
            shift 2
        ;;
        -s | --search)
            search=true
            shift 1
        ;;
        --)
            shift 1
            [ $search = true ] && search_query=$*
            break
        ;;
    esac
done

# installation - clone to /tmp and install it
if [ $install = true ]; then
    TMP_DIR="/tmp/aur-sh_$(sed -e "s/https:\/\/aur\.archlinux\.org\///; s/.git$//" <<< "$install_repo")"
    btrlog 3 "Creating the directory $TMP_DIR..."
    mkdir "$TMP_DIR" || exit 1

    btrlog 6 "Cloning $install_repo to that directoy..."
    git clone "$install_repo" "$TMP_DIR" || exit 1
    cd "$TMP_DIR" || exit 1

    btrlog 6 "Verify the build process"
    $EDITOR "./PKGBUILD"

    echo; read -n1 -p "|> Is ok to install? [Y/n] " user
    echo
    if ! [[ $user = 'n' || $user = 'N' ]]; then
        btrlog 6 "Starting the installation process..."
        makepkg -si
    fi

    echo; read -n1 -p "|> Remove tmp directory? [Y/n] " user
    echo
    if ! [[ $user = 'n' || $user = 'N' ]]; then
        btrlog 3 "Removing the $TMP_DIR directory..."
        rm -rf "$TMP_DIR"
    fi

# search - process the query and generate the link
elif [ $search = true ]; then
    QUERY=$(sed "s/ /+/g" <<< "$search_query")
    xdg-open "https://aur.archlinux.org/packages?O=0&SeB=nd&K=${QUERY}&outdated=&SB=p&SO=d&PP=50&submit=Go" || exit 1
fi
